package de.oncampus.scrumtracker

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SprintTest {

    @Autowired
    private lateinit var client: TestRestTemplate

    @Autowired
    private lateinit var sprintsRepository: SprintsRepository

    @Autowired
    private lateinit var userStoryRepository: UserStoryRepository

    @BeforeEach
    fun cleanDb() {
        userStoryRepository.deleteAll()
        sprintsRepository.deleteAll()
    }

    @Nested
    inner class GetSprints {

        @Test
        fun `return sprints for project`() {
            // given
            val sprint1 = sprintsRepository.save(Sprint("project-1", 1))
            val sprint2 = sprintsRepository.save(Sprint("project-1", 2))
            sprintsRepository.save(Sprint("project-2", 1))

            // when
            val response = client.getForEntity("/api/v1/projects/project-1/sprints", Array<Sprint>::class.java)

            // then
            assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
            assertThat(response.body).containsExactly(sprint2, sprint1)
        }
    }

    @Nested
    inner class GetCurrentSprint {

        @Test
        fun `returns current sprint for project`() {
            // given
            sprintsRepository.save(Sprint("project-1", 1))
            val sprint2 = sprintsRepository.save(Sprint("project-1", 2))
            sprintsRepository.save(Sprint("project-2", 3))

            val userStory = userStoryRepository.save(UserStory(
                    projectId = sprint2.projectId,
                    storyNumber = 1,
                    requester = "Ivan",
                    title = "foo",
                    description = "bar",
                    sprintNumber = sprint2.sprintNumber
            ))
            userStoryRepository.save(UserStory(
                    projectId = sprint2.projectId,
                    storyNumber = 2,
                    requester = "Ivan",
                    title = "baz",
                    description = "quz",
                    sprintNumber = 1
            ))

            // when
            val response = client.getForEntity("/api/v1/projects/project-1/sprints/current", SprintResponse::class.java)

            // then
            assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
            assertThat(response.body).isEqualTo(SprintResponse(
                    sprintNumber = sprint2.sprintNumber,
                    projectId = sprint2.projectId,
                    userStories = listOf(
                            UserStoryResponse(
                                    storyNumber = 1,
                                    requester = "Ivan",
                                    title = "foo",
                                    description = "bar",
                                    sprintNumber = sprint2.sprintNumber,
                                    tasks = emptyList()
                            )
                    )
            ))
        }
    }

    @Nested
    inner class CreateSprint {

        @Test
        fun `create sprint for project`() {
            // given
            sprintsRepository.save(Sprint("project-1", 1))
            val request = SprintCreateRequest(2)

            // when
            val response = client.postForEntity(
                    "/api/v1/projects/project-1/sprints",
                    request,
                    Sprint::class.java
            )

            // then
            assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)
            assertThat(response.body).isEqualTo(Sprint("project-1", 2))

            val createdSprint = sprintsRepository.getOne(SprintId("project-1", 2))
            assertThat(createdSprint).isEqualTo(Sprint("project-1", 2))
        }

        @Test
        fun `sprint number exists - BAD REQUEST`() {
            // given
            sprintsRepository.save(Sprint("project-1", 1))
            val request = SprintCreateRequest(1)

            // when
            val response = client.postForEntity(
                    "/api/v1/projects/project-1/sprints",
                    request,
                    String::class.java
            )

            // then
            assertThat(response.statusCode).isEqualTo(HttpStatus.BAD_REQUEST)
            assertThat(response.body).contains("Latest sprint of project project-1 is 1," +
                    " cannot create sprint 1")

        }
    }

    @Nested
    inner class AddStoryToSprint {

        @Test
        fun `add story to sprint - sets sprintNumber to story`() {
            // given
            val sprint = sprintsRepository.save(Sprint(
                    projectId = "project-1",
                    sprintNumber = 1
            ))
            val userStory = userStoryRepository.save(UserStory(
                    projectId = sprint.projectId,
                    storyNumber = 1,
                    requester = "Ivan",
                    title = "foo",
                    description = "bar",
                    sprintNumber = null
            ))

            // when
            val response = client.postForEntity(
                    "/api/v1/projects/project-1/sprints/1/stories",
                    AddStoryRequest(userStory.storyNumber, userStory.projectId),
                    String::class.java
            )

            // then
            val storyInDB = userStoryRepository.getOne(UserStoryId(userStory.projectId, userStory.storyNumber))
            assertThat(storyInDB.sprintNumber).isEqualTo(sprint.sprintNumber)
        }

    }

    @Nested
    inner class RemoveStoryFromSprint {

        @Test
        fun `remove story from sprint - sets sprintNumber to null`() {
            // given
            val userStory = userStoryRepository.save(UserStory(
                    projectId = "project-1",
                    storyNumber = 1,
                    requester = "Ivan",
                    title = "foo",
                    description = "bar",
                    sprintNumber = 1
            ))

            // when
            client.delete(
                    "/api/v1/projects/project-1/sprints/1/stories/1"
            )

            // then
            val storyInDB = userStoryRepository.getOne(UserStoryId(userStory.projectId, userStory.storyNumber))
            assertThat(storyInDB.sprintNumber).isNull()
        }

    }
}

