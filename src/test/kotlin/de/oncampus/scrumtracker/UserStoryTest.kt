package de.oncampus.scrumtracker

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class UserStoryTest {

    @Autowired
    private lateinit var client: TestRestTemplate

    @Autowired
    private lateinit var repository: UserStoryRepository

    @BeforeEach
    fun cleanDB() {
        repository.deleteAll()
    }

    /**
     * - Startet mit leerer Datenbank
     * - Macht einen POST request zum Anlegen einer Story
     * - Prüft, dass der Statuscode 201 zurückkommt
     * - Prüft, dass die erste (einzige) Story in der Datenbank die erwarteten Werte aus dem Create-Request hat
     */
    @Test
    fun `create first story - 201 CREATED`() {
        // given
        val projectId = "project-1"
        val request = StoryCreateRequest(
                title = "My first story",
                description = "Text describing the story"
        )

        // when
        val response = client.postForEntity(
                "/api/v1/projects/$projectId/stories",
                request,
                Void::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)
        assertThat(repository.findAll()[0]).isEqualToIgnoringGivenFields(
                UserStory(
                        projectId = projectId,
                        storyNumber = 1L,
                        requester = "testUser",
                        title = request.title,
                        description = request.description
                ),
                "tasks"
        )
    }

    @Test
    fun `create second story - increase story number`() {
        // given
        val projectId = "project-1"
        val existingStory = repository.save(UserStory(
                projectId = projectId,
                storyNumber = 1L,
                requester = "Donald Duck",
                title = "My first story",
                description = "Text describing the story"
        ))
        val request = StoryCreateRequest(
                title = "My second story",
                description = "Text describing the story"
        )

        // when
        val response = client.postForEntity(
                "/api/v1/projects/$projectId/stories",
                request,
                Void::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.CREATED)
        assertThat(repository.findAll()[0]).isEqualToIgnoringGivenFields(
                existingStory,"tasks"
        )
        assertThat(repository.findAll()[1]).isEqualToIgnoringGivenFields(
                UserStory(
                        projectId = projectId,
                        storyNumber = 2L,
                        requester = "testUser",
                        title = request.title,
                        description = request.description
                ),"tasks"
        )
    }

    @Test
    fun `get stories for project - 200 OK`() {
        // given
        val projectId = "project-1"
        val existingStory = repository.save(UserStory(
                projectId = projectId,
                storyNumber = 1L,
                requester = "Donald Duck",
                title = "My first story",
                description = "Text describing the story"
        ))
        val storyForOtherProject = repository.save(UserStory(
                projectId = "project-2",
                storyNumber = 1L,
                requester = "Mickey Mouse",
                title = "My first story",
                description = "Text describing the story"
        ))

        // when
        val response = client.getForEntity(
                "/api/v1/projects/$projectId/stories",
                Array<UserStory>::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body).containsExactlyInAnyOrder(existingStory)
        assertThat(response.body).doesNotContain(storyForOtherProject)
    }

    @Test
    fun `get unassigned stories for project - filters stories with sprintId`() {
        // given
        val projectId = "project-1"
        val existingStory = repository.save(UserStory(
                projectId = projectId,
                storyNumber = 1L,
                requester = "Donald Duck",
                title = "My first story",
                description = "Text describing the story"
        ))
        val storyWithSprintId = repository.save(UserStory(
                projectId = projectId,
                storyNumber = 2L,
                requester = "Mickey Mouse",
                title = "My second story",
                description = "Text describing the story",
                sprintNumber = 1L
        ))

        // when
        val response = client.getForEntity(
                "/api/v1/projects/$projectId/stories?assignedToSprint=false",
                Array<UserStory>::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body).containsExactlyInAnyOrder(existingStory)
        assertThat(response.body).doesNotContain(storyWithSprintId)
    }

    @Test
    fun `get story by id`() {
        // given
        val projectId = "project-1"
        val story1 = repository.save(UserStory(
                projectId = projectId,
                storyNumber = 1L,
                requester = "Donald Duck",
                title = "My first story",
                description = "Text describing the story",
                tasks = listOf(Task("do stuff"))
        ))

        // when
        val response = client.getForEntity(
                "/api/v1/projects/$projectId/stories/1",
                UserStory::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(response.body).isEqualTo(story1)
    }

    @Test
    fun `update story that doesn't exist - NOT FOUND`() {
        // given
        val projectId = "project-1"

        // when
        val response = client.exchange(
                "/api/v1/projects/$projectId/stories/1",
                HttpMethod.PUT,
                HttpEntity(StoryUpdateRequest(
                        title = "foo",
                        description = "bar",
                        tasks = emptyList()
                )),
                String::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
    }

    @Test
    fun `update story - saves title and description`() {
        // given
        val projectId = "project-1"
        val story1 = repository.save(UserStory(
                projectId = projectId,
                storyNumber = 1L,
                requester = "Donald Duck",
                title = "My first story",
                description = "Text describing the story"
        ))

        // when
        val updateRequest = StoryUpdateRequest(
                title = "foo",
                description = "bar",
                tasks = emptyList()
        )
        val response = client.exchange(
                "/api/v1/projects/$projectId/stories/1",
                HttpMethod.PUT,
                HttpEntity(updateRequest),
                UserStory::class.java
        )

        // then
        assertThat(response.statusCode).isEqualTo(HttpStatus.OK)

        val updatedStory = repository.getOne(UserStoryId(projectId, 1L))
        assertThat(updatedStory.title).isEqualTo(updateRequest.title)
        assertThat(updatedStory.description).isEqualTo(updateRequest.description)
    }

    @Test
    fun `update story - updates tasks`() {
        // given
        val projectId = "project-1"
        val story1 = repository.save(UserStory(
                projectId = projectId,
                storyNumber = 1L,
                requester = "Donald Duck",
                title = "My first story",
                description = "Text describing the story",
                tasks = listOf(
                        Task("foo", true),
                        Task("bar", false)
                )
        ))

        // when
        val updateRequest = StoryUpdateRequest(
                title = "foo",
                description = "bar",
                tasks = listOf(
                        TaskRequest("foo", true),
                        TaskRequest("quz", true)
                )
        )
        val response = client.exchange(
                "/api/v1/projects/$projectId/stories/1",
                HttpMethod.PUT,
                HttpEntity(updateRequest),
                UserStory::class.java
        )

        // then
        val updatedStory = repository.getOne(UserStoryId(projectId, 1L))
        assertThat(updatedStory.tasks).containsExactlyInAnyOrder(
                Task("foo", true),
                Task("quz", true)
        )
    }
}
