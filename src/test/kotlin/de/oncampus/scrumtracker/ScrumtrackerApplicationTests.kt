package de.oncampus.scrumtracker

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

/**
 * Testet ob Anwendung starten kann, keine Framework Errors von Spring, JPA etc
 */
@SpringBootTest
class ScrumtrackerApplicationTests {

    @Test
    fun contextLoads() {
    }

}
