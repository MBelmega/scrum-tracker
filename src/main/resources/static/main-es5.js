function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div id=\"app\">\r\n  <app-header></app-header>\r\n\r\n  <main class=\"container\" *ngIf=\"userIsLoggedIn()\">\r\n    <nav mat-tab-nav-bar>\r\n      <ng-container *ngFor=\"let link of navLinks\">\r\n        <a mat-tab-link\r\n           [routerLink]=\"link.path\"\r\n           routerLinkActive #rla=\"routerLinkActive\"\r\n           [active]=\"rla.isActive\">\r\n          {{link.label}}\r\n        </a>\r\n      </ng-container>\r\n    </nav>\r\n\r\n    <router-outlet></router-outlet>\r\n  </main>\r\n\r\n  <main class=\"container\" *ngIf=\"!userIsLoggedIn()\">\r\n    Please <a class=\"link\" (click)=\"signInWithGoogle()\">log in</a> to get started.\r\n  </main>\r\n\r\n  <app-footer></app-footer>\r\n</div>\r\n\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/create-story/create-story.component.html":
  /*!***********************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/create-story/create-story.component.html ***!
    \***********************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsCreateStoryCreateStoryComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"mt-5\">\r\n  <h3>New story</h3>\r\n\r\n  <form class=\"create-story-form\" [formGroup]=\"form\" (ngSubmit)=\"onSubmit()\">\r\n    <mat-form-field class=\"create-story-full-width\">\r\n      <input matInput placeholder=\"Title\" formControlName=\"storyTitle\">\r\n    </mat-form-field>\r\n\r\n    <mat-form-field class=\"create-story-full-width\">\r\n      <textarea matInput\r\n                cdkTextareaAutosize\r\n                #autosize=\"cdkTextareaAutosize\"\r\n                placeholder=\"Description\"\r\n                formControlName=\"storyDescription\"></textarea>\r\n    </mat-form-field>\r\n    <div class=\"mt-2\">\r\n      <button mat-stroked-button (click)=\"form.reset()\"\r\n              class=\"mr-2\">\r\n        Reset\r\n      </button>\r\n      <button mat-stroked-button color=\"primary\" type=\"submit\" [disabled]=\"!form.valid\">\r\n        Submit\r\n      </button>\r\n    </div>\r\n  </form>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/current-sprint/current-sprint.component.html":
  /*!***************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/current-sprint/current-sprint.component.html ***!
    \***************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsCurrentSprintCurrentSprintComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


      __webpack_exports__["default"] = "<div class=\"mt-5\" *ngIf=\"currentSprint\">\r\n  <h2>Current Sprint (#{{currentSprint.sprintNumber}})</h2>\r\n\r\n  <table mat-table [dataSource]=\"currentSprint.userStories\">\r\n\r\n    <ng-container matColumnDef=\"storyNumber\">\r\n      <th mat-header-cell *matHeaderCellDef>Story No.</th>\r\n      <td mat-cell *matCellDef=\"let element\"> {{element.storyNumber}} </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"title\">\r\n      <th mat-header-cell *matHeaderCellDef>Title</th>\r\n      <td mat-cell *matCellDef=\"let element\">\r\n        <a [routerLink]=\"['/projects', projectId, 'stories', element.storyNumber]\">{{element.title}}</a></td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"actions\">\r\n      <th mat-header-cell *matHeaderCellDef></th>\r\n      <td mat-cell *matCellDef=\"let element\">\r\n        <button mat-stroked-button (click)=\"removeFromCurrentSprint(element)\">Remove</button>\r\n      </td>\r\n    </ng-container>\r\n\r\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n    <tr mat-row *matRowDef=\"let story; columns: displayedColumns;\"></tr>\r\n  </table>\r\n</div>\r\n\r\n<div class=\"mt-5\">\r\n  <button mat-stroked-button color=\"primary\" (click)=\"startNextSprint()\">Start next Sprint</button>\r\n</div>\r\n\r\n<div class=\"mt-5\" *ngIf=\"freeStories\">\r\n  <h2>User Stories</h2>\r\n\r\n  <table mat-table [dataSource]=\"freeStories\">\r\n\r\n    <ng-container matColumnDef=\"storyNumber\">\r\n      <th mat-header-cell *matHeaderCellDef>Story No.</th>\r\n      <td mat-cell *matCellDef=\"let element\"> {{element.storyNumber}} </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"title\">\r\n      <th mat-header-cell *matHeaderCellDef>Title</th>\r\n      <td mat-cell *matCellDef=\"let element\">\r\n        <a [routerLink]=\"['/projects', projectId, 'stories', element.storyNumber]\">{{element.title}}</a>\r\n      </td>\r\n    </ng-container>\r\n    <ng-container matColumnDef=\"actions\">\r\n      <th mat-header-cell *matHeaderCellDef></th>\r\n      <td mat-cell *matCellDef=\"let element\">\r\n        <button mat-stroked-button (click)=\"addToCurrentSprint(element)\">Add</button>\r\n      </td>\r\n    </ng-container>\r\n\r\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n    <tr mat-row *matRowDef=\"let story; columns: displayedColumns;\"></tr>\r\n  </table>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/footer/footer.component.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/footer/footer.component.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsFooterFooterComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


      __webpack_exports__["default"] = "<footer>\r\n  <a href=\"https://kotlinlang.org/\">Kotlin</a>\r\n  <a href=\"https://spring.io/projects/spring-boot\">Spring Boot</a>\r\n  <a href=\"https://angular.io/\">Angular</a>\r\n</footer>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html":
  /*!***********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsHeaderHeaderComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


      __webpack_exports__["default"] = "<mat-toolbar color=\"primary\" role=\"heading\" class=\"mat-elevation-z6 d-flex justify-content-between\">\r\n\r\n  <a mat-button routerLink=\"/\">\r\n    <svg\r\n      style=\"margin-right: 4px\"\r\n      fill=\"white\"\r\n      height=\"24px\"\r\n      viewBox=\"-1 0 478 478.77605\"\r\n      width=\"24px\" xmlns=\"http://www.w3.org/2000/svg\">\r\n      <path\r\n        d=\"m40.371094 478.777344c-16.167969-.007813-30.746094-9.75-36.9375-24.6875-6.1875-14.9375-2.777344-32.136719 8.648437-43.578125l141.425781-141.421875c3.125-3.125 8.1875-3.125 11.3125 0l45.253907 45.253906c3.125 3.125 3.125 8.1875 0 11.3125l-141.421875 141.421875c-7.496094 7.503906-17.675782 11.714844-28.28125 11.699219zm118.792968-192.722656-135.761718 135.769531c-9.242188 9.394531-9.183594 24.480469.136718 33.800781 9.316407 9.320312 24.40625 9.382812 33.800782.144531l135.769531-135.769531zm0 0\"/>\r\n      <path\r\n        d=\"m128.417969 206.785156c-16.175781-.015625-30.75-9.769531-36.929688-24.714844-6.183593-14.949218-2.75-32.148437 8.691407-43.582031l80.640624-80.761719c21.882813-21.839843 57.316407-21.839843 79.199219 0l40.703125 40.800782c7.53125 7.496094 11.765625 17.679687 11.765625 28.304687s-4.234375 20.808594-11.765625 28.304688c-15.613281 15.570312-40.882812 15.570312-56.496094 0l-23.808593-23.824219-63.78125 63.785156c-7.480469 7.488282-17.632813 11.695313-28.21875 11.6875zm92-149.441406c-10.609375-.019531-20.789063 4.191406-28.285157 11.695312l-80.699218 80.800782c-4.519532 4.492187-7.0625 10.601562-7.0625 16.976562s2.542968 12.484375 7.0625 16.976563c9.484375 9.066406 24.421875 9.066406 33.90625 0l69.441406-69.441407c3.121094-3.121093 8.1875-3.121093 11.308594 0l29.441406 29.441407c9.367188 9.335937 24.523438 9.335937 33.886719 0 4.519531-4.492188 7.058593-10.597657 7.058593-16.96875 0-6.371094-2.539062-12.476563-7.058593-16.96875l-40.710938-40.800781c-7.496093-7.511719-17.675781-11.726563-28.289062-11.710938zm0 0\"/>\r\n      <path\r\n        d=\"m435.058594 272h-110.640625c-13.253907 0-24-10.746094-24-24v-32c.003906-2.121094.84375-4.15625 2.34375-5.65625l42.347656-42.34375c4.574219-4.574219 11.453125-5.941406 17.433594-3.46875 5.976562 2.476562 9.875 8.308594 9.875 14.78125v12.6875h64c11.285156 0 22.046875 4.765625 29.628906 13.121094 7.582031 8.355468 11.285156 19.527344 10.1875 30.757812-2.511719 20.777344-20.25 36.335938-41.175781 36.121094zm-118.640625-52.6875v28.6875c0 4.417969 3.582031 8 8 8h110.640625c12.6875.273438 23.554687-9.023438 25.25-21.601562.675781-6.75-1.53125-13.46875-6.085938-18.5-4.550781-5.03125-11.019531-7.902344-17.804687-7.898438h-72c-4.417969 0-8-3.582031-8-8v-20.6875zm151.847656 15.816406\"/>\r\n      <path\r\n        d=\"m224.417969 462.800781c-16.179688-.007812-30.765625-9.765625-36.949219-24.71875-6.179688-14.957031-2.742188-32.164062 8.710938-43.59375l54.96875-54.96875-49.175782-49.121093c-11.296875-10.769532-15.28125-27.132813-10.210937-41.894532 5.074219-14.761718 18.277343-25.214844 33.808593-26.769531 11.964844-1.226563 23.84375 3 32.34375 11.503906l66.730469 66.730469c21.835938 21.882812 21.835938 57.316406 0 79.199219l-72.050781 72c-7.46875 7.472656-17.609375 11.660156-28.175781 11.632812zm5.265625-225.289062c-.800782 0-1.601563.046875-2.464844.128906-7.273438.710937-13.824219 4.714844-17.773438 10.863281-6.089843 9.828125-4.472656 22.574219 3.878907 30.566406l54.792969 54.792969c3.121093 3.125 3.121093 8.1875 0 11.3125l-60.640626 60.640625c-4.523437 4.492188-7.066406 10.601563-7.066406 16.976563 0 6.371093 2.542969 12.484375 7.066406 16.976562 9.484376 9.058594 24.417969 9.058594 33.902344 0l72-72c15.574219-15.632812 15.574219-40.914062 0-56.546875l-66.726562-66.726562c-4.515625-4.5-10.636719-7.015625-17.007813-6.984375zm0 0\"/>\r\n      <path\r\n        d=\"m404.417969 144c-39.761719 0-72-32.234375-72-72s32.238281-72 72-72c39.765625 0 72 32.234375 72 72-.046875 39.746094-32.253907 71.953125-72 72zm0-128c-30.925781 0-56 25.070312-56 56s25.074219 56 56 56c30.929687 0 56-25.070312 56-56-.035157-30.914062-25.085938-55.964844-56-56zm0 0\"/>\r\n    </svg>\r\n\r\n    <span>SprintTracker</span>\r\n  </a>\r\n  <div>\r\n    <span *ngIf=\"getUser()\">{{getUser().name}}</span>\r\n    <button *ngIf=\"!getUser()\" mat-button (click)=\"signInWithGoogle()\">Login</button>\r\n    <button *ngIf=\"getUser()\" mat-button (click)=\"signOutWithGoogle()\">Logout</button>\r\n  </div>\r\n</mat-toolbar>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/sprints-history/sprints-history.component.html":
  /*!*****************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/sprints-history/sprints-history.component.html ***!
    \*****************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsSprintsHistorySprintsHistoryComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


      __webpack_exports__["default"] = "<table class=\"mt-3\" mat-table [dataSource]=\"allStories\">\r\n\r\n  <ng-container matColumnDef=\"sprintNumber\">\r\n    <th mat-header-cell *matHeaderCellDef>Sprint</th>\r\n    <td mat-cell *matCellDef=\"let element\"> {{element.sprintNumber}} </td>\r\n  </ng-container>\r\n  <ng-container matColumnDef=\"storyNumber\">\r\n    <th mat-header-cell *matHeaderCellDef>Story No.</th>\r\n    <td mat-cell *matCellDef=\"let element\"> {{element.storyNumber}} </td>\r\n  </ng-container>\r\n  <ng-container matColumnDef=\"title\">\r\n    <th mat-header-cell *matHeaderCellDef>Title</th>\r\n    <td mat-cell *matCellDef=\"let element\">\r\n      <a [routerLink]=\"['/projects', projectId, 'stories', element.storyNumber]\">{{element.title}}</a></td>\r\n  </ng-container>\r\n\r\n  <tr mat-header-row *matHeaderRowDef=\"displayedColumns\"></tr>\r\n  <tr mat-row *matRowDef=\"let story; columns: displayedColumns;\"></tr>\r\n</table>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/components/story-details/story-details.component.html":
  /*!*************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/story-details/story-details.component.html ***!
    \*************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppComponentsStoryDetailsStoryDetailsComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


      __webpack_exports__["default"] = "<div *ngIf=\"story\" class=\"mt-5\">\r\n  <h3>{{story.title}}</h3>\r\n  <div>\r\n    <label>Requester: </label> <span>{{story.requester}}</span>\r\n  </div>\r\n  <div>\r\n    <label>Sprint: </label> <span>{{story.sprintNumber}}</span>\r\n  </div>\r\n  <div>\r\n    <label>Description:</label>\r\n    <p>{{story.description}}</p>\r\n  </div>\r\n  <div class=\"mt-5 tasks-form\">\r\n    <h4>Tasks</h4>\r\n    <div *ngFor=\"let task of tasks; index as i\" >\r\n      <div *ngIf=\"!task.done\" class=\"d-flex align-items-center\">\r\n        <mat-form-field class=\"flex-grow-1\">\r\n          <input matInput value=\"{{task.title}}\">\r\n        </mat-form-field>\r\n        <button mat-button class=\"flex-grow-0 ml-2\" (click)=\"markTaskAsDone(i, true)\">\r\n          Done\r\n        </button>\r\n        <button mat-button class=\"flex-grow-0 ml-2\" (click)=\"deleteTask(i)\">\r\n          Delete\r\n        </button>\r\n      </div>\r\n      <div *ngIf=\"task.done\" class=\"d-flex align-items-center\">\r\n        <mat-form-field class=\"flex-grow-1\">\r\n          <input matInput class=\"task-done\" disabled value=\"{{task.title}}\">\r\n        </mat-form-field>\r\n      </div>\r\n    </div>\r\n    <div>\r\n      <mat-form-field class=\"tasks-full-width\">\r\n        <input matInput #newTask placeholder=\"New task\"\r\n               [(ngModel)]=\"newTaskTitle\"\r\n               (keydown.enter)=\"addNewTask()\"\r\n               (focus)=\"focusState = true\"\r\n               (focusout)=\"focusState = false\"\r\n               autocomplete=\"off\"\r\n        >\r\n        <mat-hint *ngIf=\"focusState\" align=\"start\">Press ENTER to add</mat-hint>\r\n      </mat-form-field>\r\n    </div>\r\n    <div class=\"mt-2\">\r\n      <button mat-stroked-button (click)=\"resetTasks()\"\r\n              class=\"mr-2\">\r\n        Reset\r\n      </button>\r\n      <button mat-stroked-button color=\"primary\" (click)=\"saveTasks()\">\r\n        Save\r\n      </button>\r\n    </div>\r\n  </div>\r\n</div>\r\n";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var m = typeof Symbol === "function" && o[Symbol.iterator],
          i = 0;
      if (m) return m.call(o);
      return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result.default = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        default: mod
      };
    }
    /***/

  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _components_story_details_story_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./components/story-details/story-details.component */
    "./src/app/components/story-details/story-details.component.ts");
    /* harmony import */


    var _components_sprints_history_sprints_history_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./components/sprints-history/sprints-history.component */
    "./src/app/components/sprints-history/sprints-history.component.ts");
    /* harmony import */


      var _components_create_story_create_story_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
          /*! ./components/create-story/create-story.component */
          "./src/app/components/create-story/create-story.component.ts");
      /* harmony import */


      var _components_current_sprint_current_sprint_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
          /*! ./components/current-sprint/current-sprint.component */
          "./src/app/components/current-sprint/current-sprint.component.ts");
      /**
       * Definiert welche UI-Komponente im Main-Bereich der app-component.html angezeigt wird,
       * abhängig von der aktiven Route. Navigation kann so über Links erfolgen.
       */


      var routes = [{
          path: '',
          redirectTo: 'projects/1/sprints/current',
          pathMatch: 'full'
      }, {
          path: 'projects/:projectId/sprints/current',
          component: _components_current_sprint_current_sprint_component__WEBPACK_IMPORTED_MODULE_6__["CurrentSprintComponent"]
      }, {
          path: 'projects/:projectId/sprints/history',
          component: _components_sprints_history_sprints_history_component__WEBPACK_IMPORTED_MODULE_4__["SprintsHistoryComponent"]
      }, {
          path: 'projects/:projectId/stories/create',
          component: _components_create_story_create_story_component__WEBPACK_IMPORTED_MODULE_5__["CreateStoryComponent"]
      }, {
          path: 'projects/:projectId/stories/:storyId',
          component: _components_story_details_story_details_component__WEBPACK_IMPORTED_MODULE_3__["StoryDetailsComponent"]
      }];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.scss":
  /*!************************************!*\
    !*** ./src/app/app.component.scss ***!
    \************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "#app {\n  display: flex;\n  flex-direction: column;\n  min-height: 100vh;\n}\n\nmain {\n  flex-grow: 1;\n  margin-top: 32px;\n}\n\na.link {\n  color: #304ffe;\n}\n\na.link:hover {\n  text-decoration: underline;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvRTpcXHdvcmtzcGFjZVxcc2NydW10cmFja2VyXFxzY3J1bXRyYWNrZXItZnJvbnRlbmQvc3JjXFxhcHBcXGFwcC5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7QUNDRjs7QURFQTtFQUNFLFlBQUE7RUFDQSxnQkFBQTtBQ0NGOztBREVBO0VBQ0UsY0FBQTtBQ0NGOztBREVBO0VBQ0UsMEJBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNhcHAge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICBtaW4taGVpZ2h0OiAxMDB2aDtcclxufVxyXG5cclxubWFpbiB7XHJcbiAgZmxleC1ncm93OiAxO1xyXG4gIG1hcmdpbi10b3A6IDMycHg7XHJcbn1cclxuXHJcbmEubGluayB7XHJcbiAgY29sb3I6ICMzMDRmZmU7XHJcbn1cclxuXHJcbmEubGluazpob3ZlciB7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcbn1cclxuIiwiI2FwcCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIG1pbi1oZWlnaHQ6IDEwMHZoO1xufVxuXG5tYWluIHtcbiAgZmxleC1ncm93OiAxO1xuICBtYXJnaW4tdG9wOiAzMnB4O1xufVxuXG5hLmxpbmsge1xuICBjb2xvcjogIzMwNGZmZTtcbn1cblxuYS5saW5rOmhvdmVyIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./services/user.service */
    "./src/app/services/user.service.ts");

    var AppComponent =
    /*#__PURE__*/
    function () {
      function AppComponent(userService) {
        _classCallCheck(this, AppComponent);

        this.userService = userService;
        this.projectId = '1';
        this.navLinks = [{
          path: 'projects/' + this.projectId + '/sprints/current',
          label: 'Current Planning'
        }, {
          path: 'projects/' + this.projectId + '/sprints/history',
          label: 'Sprint History'
        }, {
          path: 'projects/' + this.projectId + '/stories/create',
          label: 'Create New Story'
        }];
      }

      _createClass(AppComponent, [{
        key: "userIsLoggedIn",
        value: function userIsLoggedIn() {
          return !!this.userService.user;
        }
      }, {
        key: "signInWithGoogle",
        value: function signInWithGoogle() {
          this.userService.signInWithGoogle();
        }
      }]);

      return AppComponent;
    }();

    AppComponent.ctorParameters = function () {
      return [{
        type: _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]
      }];
    };

    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/app.component.scss")).default]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: provideConfig, AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "provideConfig", function () {
      return provideConfig;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./components/footer/footer.component */
    "./src/app/components/footer/footer.component.ts");
    /* harmony import */


    var _components_header_header_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./components/header/header.component */
    "./src/app/components/header/header.component.ts");
    /* harmony import */


    var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @angular/platform-browser/animations */
    "./node_modules/@angular/platform-browser/fesm2015/animations.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _components_story_details_story_details_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./components/story-details/story-details.component */
    "./src/app/components/story-details/story-details.component.ts");
    /* harmony import */


    var _components_sprints_history_sprints_history_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./components/sprints-history/sprints-history.component */
    "./src/app/components/sprints-history/sprints-history.component.ts");
    /* harmony import */


    var _components_current_sprint_current_sprint_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./components/current-sprint/current-sprint.component */
    "./src/app/components/current-sprint/current-sprint.component.ts");
    /* harmony import */


    var _components_create_story_create_story_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! ./components/create-story/create-story.component */
    "./src/app/components/create-story/create-story.component.ts");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var angularx_social_login__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! angularx-social-login */
    "./node_modules/angularx-social-login/angularx-social-login.js");
    /* harmony import */


    var _http_outgoing_interceptor__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./http-outgoing.interceptor */
    "./src/app/http-outgoing.interceptor.ts");

    var config = new angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["AuthServiceConfig"]([{
      id: angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["GoogleLoginProvider"].PROVIDER_ID,
        provider: new angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["GoogleLoginProvider"]('410600106727-c8l3pilsj8ln9co41jf6c9ma7tgf703i.apps.googleusercontent.com')
    }]);

    function provideConfig() {
      return config;
    }

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"], _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_5__["FooterComponent"], _components_header_header_component__WEBPACK_IMPORTED_MODULE_6__["HeaderComponent"], _components_story_details_story_details_component__WEBPACK_IMPORTED_MODULE_10__["StoryDetailsComponent"], _components_sprints_history_sprints_history_component__WEBPACK_IMPORTED_MODULE_11__["SprintsHistoryComponent"], _components_current_sprint_current_sprint_component__WEBPACK_IMPORTED_MODULE_12__["CurrentSprintComponent"], _components_create_story_create_story_component__WEBPACK_IMPORTED_MODULE_13__["CreateStoryComponent"]],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HttpClientModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["BrowserAnimationsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatToolbarModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatTabsModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatSnackBarModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"], angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["SocialLoginModule"], _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatIconModule"]],
      providers: [{
        provide: angularx_social_login__WEBPACK_IMPORTED_MODULE_15__["AuthServiceConfig"],
        useFactory: provideConfig
      }, {
        provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_9__["HTTP_INTERCEPTORS"],
        useClass: _http_outgoing_interceptor__WEBPACK_IMPORTED_MODULE_16__["TokenInterceptor"],
        multi: true
      }],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/components/create-story/create-story.component.scss":
  /*!*********************************************************************!*\
    !*** ./src/app/components/create-story/create-story.component.scss ***!
    \*********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsCreateStoryCreateStoryComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".create-story-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.create-story-full-width {\n  width: 100%;\n}\n\ntextarea {\n  box-sizing: content-box;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jcmVhdGUtc3RvcnkvRTpcXHdvcmtzcGFjZVxcc2NydW10cmFja2VyXFxzY3J1bXRyYWNrZXItZnJvbnRlbmQvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNyZWF0ZS1zdG9yeVxcY3JlYXRlLXN0b3J5LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2NyZWF0ZS1zdG9yeS9jcmVhdGUtc3RvcnkuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQ0NGOztBREVBO0VBQ0UsV0FBQTtBQ0NGOztBREVBO0VBQ0UsdUJBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY3JlYXRlLXN0b3J5L2NyZWF0ZS1zdG9yeS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jcmVhdGUtc3RvcnktZm9ybSB7XHJcbiAgbWluLXdpZHRoOiAxNTBweDtcclxuICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uY3JlYXRlLXN0b3J5LWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG50ZXh0YXJlYSB7XHJcbiAgYm94LXNpemluZzogY29udGVudC1ib3g7XHJcbn1cclxuIiwiLmNyZWF0ZS1zdG9yeS1mb3JtIHtcbiAgbWluLXdpZHRoOiAxNTBweDtcbiAgbWF4LXdpZHRoOiA1MDBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jcmVhdGUtc3RvcnktZnVsbC13aWR0aCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG50ZXh0YXJlYSB7XG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/components/create-story/create-story.component.ts":
  /*!*******************************************************************!*\
    !*** ./src/app/components/create-story/create-story.component.ts ***!
    \*******************************************************************/

  /*! exports provided: CreateStoryComponent */

  /***/
  function srcAppComponentsCreateStoryCreateStoryComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CreateStoryComponent", function () {
      return CreateStoryComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


      var _services_user_story_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
          /*! ../../services/user-story.service */
          "./src/app/services/user-story.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
          /*! @angular/router */
          "./node_modules/@angular/router/fesm2015/router.js");
      /**
       * Formular zum Anlegen einer neuen Story
       */


      var CreateStoryComponent =
          /*#__PURE__*/
          function () {
              function CreateStoryComponent(fb, userStoryService, route) {
                  var _this = this;

                  _classCallCheck(this, CreateStoryComponent);

                  this.fb = fb;
                  this.userStoryService = userStoryService;
                  this.route = route;
                  this.form = this.fb.group({
                      storyTitle: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                      storyDescription: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
                  });
                  route.params.subscribe(function (params) {
                      _this.projectId = _this.route.snapshot.params['projectId'];
                  });
              }

              _createClass(CreateStoryComponent, [{
                  key: "ngOnInit",
                  value: function ngOnInit() {
                  } // Bei Submit-Klick, schicke Request zum Erstellen der Story ans Backend und resette Formular

              }, {
                  key: "onSubmit",
                  value: function onSubmit() {
                      var _this2 = this;

                      var formValue = this.form.getRawValue();
                      this.userStoryService.createStory(this.projectId, {
                          title: formValue.storyTitle,
                          description: formValue.storyDescription
                      }).subscribe(function () {
                          _this2.form.reset();
                      });
                  }
              }]);

              return CreateStoryComponent;
          }();

    CreateStoryComponent.ctorParameters = function () {
      return [{
        type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
      }, {
        type: _services_user_story_service__WEBPACK_IMPORTED_MODULE_3__["UserStoryService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
      }];
    };

    CreateStoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-create-story',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./create-story.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/create-story/create-story.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./create-story.component.scss */
      "./src/app/components/create-story/create-story.component.scss")).default]
    })], CreateStoryComponent);
    /***/
  },

  /***/
  "./src/app/components/current-sprint/current-sprint.component.scss":
  /*!*************************************************************************!*\
    !*** ./src/app/components/current-sprint/current-sprint.component.scss ***!
    \*************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsCurrentSprintCurrentSprintComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "table td.mat-column-storyNumber {\n  width: 10%;\n}\n\ntable td.mat-column-actions {\n  width: 20%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jdXJyZW50LXNwcmludC9FOlxcd29ya3NwYWNlXFxzY3J1bXRyYWNrZXJcXHNjcnVtdHJhY2tlci1mcm9udGVuZC9zcmNcXGFwcFxcY29tcG9uZW50c1xcY3VycmVudC1zcHJpbnRcXGN1cnJlbnQtc3ByaW50LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2N1cnJlbnQtc3ByaW50L2N1cnJlbnQtc3ByaW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsVUFBQTtBQ0NGOztBREVBO0VBQ0UsVUFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jdXJyZW50LXNwcmludC9jdXJyZW50LXNwcmludC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHRkLm1hdC1jb2x1bW4tc3RvcnlOdW1iZXIge1xyXG4gIHdpZHRoOiAxMCU7XHJcbn1cclxuXHJcbnRhYmxlIHRkLm1hdC1jb2x1bW4tYWN0aW9ucyB7XHJcbiAgd2lkdGg6IDIwJTtcclxufVxyXG4iLCJ0YWJsZSB0ZC5tYXQtY29sdW1uLXN0b3J5TnVtYmVyIHtcbiAgd2lkdGg6IDEwJTtcbn1cblxudGFibGUgdGQubWF0LWNvbHVtbi1hY3Rpb25zIHtcbiAgd2lkdGg6IDIwJTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/components/current-sprint/current-sprint.component.ts":
  /*!***********************************************************************!*\
    !*** ./src/app/components/current-sprint/current-sprint.component.ts ***!
    \***********************************************************************/

  /*! exports provided: CurrentSprintComponent */

  /***/
  function srcAppComponentsCurrentSprintCurrentSprintComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CurrentSprintComponent", function () {
      return CurrentSprintComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_user_story_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/user-story.service */
    "./src/app/services/user-story.service.ts");
    /* harmony import */


      var _services_sprint_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
          /*! ../../services/sprint.service */
          "./src/app/services/sprint.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
          /*! @angular/router */
          "./node_modules/@angular/router/fesm2015/router.js");
      /**
       * Zeigt zwei Tabellen, eine für Aktuellen Sprint und eine für nicht zugeordnete Stories.
       * Nutzer kann Stories aus der zweiten Tabelle dem Sprint zuordnen
       */


      var CurrentSprintComponent =
          /*#__PURE__*/
          function () {
              function CurrentSprintComponent(userStoryService, sprintService, route) {
                  var _this3 = this;

                  _classCallCheck(this, CurrentSprintComponent);

                  this.userStoryService = userStoryService;
                  this.sprintService = sprintService;
                  this.route = route;
                  this.displayedColumns = ['storyNumber', 'title', 'actions'];
                  route.params.subscribe(function (params) {
                      _this3.projectId = _this3.route.snapshot.params['projectId'];
                  });
              }

              _createClass(CurrentSprintComponent, [{
                  key: "ngOnInit",
                  value: function ngOnInit() {
                      this.loadCurrentSprint();
                      this.loadFreeStories();
                  }
              }, {
                  key: "loadFreeStories",
                  value: function loadFreeStories() {
                      var _this4 = this;

                      this.userStoryService.getStories({
                          projectId: this.projectId,
                          assignedToSprint: false
                      }).subscribe(function (stories) {
                          _this4.freeStories = stories;
                      });
                  }
              }, {
                  key: "loadCurrentSprint",
                  value: function loadCurrentSprint() {
                      var _this5 = this;

                      this.sprintService.getCurrentSprint({
                          projectId: this.projectId
                      }).subscribe(function (sprint) {
                          _this5.currentSprint = sprint;
                      });
                  }
              }, {
                  key: "addToCurrentSprint",
                  value: function addToCurrentSprint(element) {
                      var _this6 = this;

                      this.sprintService.addStoryToSprint({
                          sprint: this.currentSprint,
                          story: element
                      }).subscribe(function () {
                          _this6.loadCurrentSprint();

                          _this6.loadFreeStories();
                      });
                  }
              }, {
                  key: "removeFromCurrentSprint",
                  value: function removeFromCurrentSprint(element) {
                      var _this7 = this;

                      this.sprintService.removeStoryFromSprint({
                          sprint: this.currentSprint,
                          story: element
                      }).subscribe(function () {
                          _this7.loadCurrentSprint();

                          _this7.loadFreeStories();
                      });
                  }
              }, {
                  key: "startNextSprint",
                  value: function startNextSprint() {
                      var _this8 = this;

                      this.sprintService.createNewSprint({
                          sprint: this.currentSprint
                      }).subscribe(function () {
                          _this8.loadCurrentSprint();
                      });
                  }
              }]);

              return CurrentSprintComponent;
          }();

    CurrentSprintComponent.ctorParameters = function () {
      return [{
        type: _services_user_story_service__WEBPACK_IMPORTED_MODULE_2__["UserStoryService"]
      }, {
        type: _services_sprint_service__WEBPACK_IMPORTED_MODULE_3__["SprintService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"]
      }];
    };

    CurrentSprintComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-current-sprint',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./current-sprint.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/current-sprint/current-sprint.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./current-sprint.component.scss */
      "./src/app/components/current-sprint/current-sprint.component.scss")).default]
    })], CurrentSprintComponent);
    /***/
  },

  /***/
  "./src/app/components/footer/footer.component.scss":
  /*!*********************************************************!*\
    !*** ./src/app/components/footer/footer.component.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsFooterFooterComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ":host {\n  display: flex;\n  justify-content: center;\n  position: relative;\n  font-size: 14px;\n  margin-top: 100px;\n  padding: 24px 40px;\n  color: #2f3538;\n  text-align: right;\n}\n\na + a {\n  margin-left: 32px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mb290ZXIvRTpcXHdvcmtzcGFjZVxcc2NydW10cmFja2VyXFxzY3J1bXRyYWNrZXItZnJvbnRlbmQvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGZvb3RlclxcZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2Zvb3Rlci9mb290ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFFQSxpQkFBQTtFQUNBLGtCQUFBO0VBRUEsY0FBQTtFQUNBLGlCQUFBO0FDRkY7O0FETUE7RUFDRSxpQkFBQTtBQ0hGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbjpob3N0IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBmb250LXNpemU6IDE0cHg7XHJcblxyXG4gIG1hcmdpbi10b3A6IDEwMHB4O1xyXG4gIHBhZGRpbmc6IDI0cHggNDBweDtcclxuXHJcbiAgY29sb3I6ICMyZjM1Mzg7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcblxyXG59XHJcblxyXG5hICsgYSB7XHJcbiAgbWFyZ2luLWxlZnQ6IDMycHg7XHJcbn1cclxuIiwiOmhvc3Qge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDE0cHg7XG4gIG1hcmdpbi10b3A6IDEwMHB4O1xuICBwYWRkaW5nOiAyNHB4IDQwcHg7XG4gIGNvbG9yOiAjMmYzNTM4O1xuICB0ZXh0LWFsaWduOiByaWdodDtcbn1cblxuYSArIGEge1xuICBtYXJnaW4tbGVmdDogMzJweDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/components/footer/footer.component.ts":
  /*!*******************************************************!*\
    !*** ./src/app/components/footer/footer.component.ts ***!
    \*******************************************************/

  /*! exports provided: FooterComponent */

  /***/
  function srcAppComponentsFooterFooterComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FooterComponent", function () {
      return FooterComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var FooterComponent =
    /*#__PURE__*/
    function () {
      function FooterComponent() {
        _classCallCheck(this, FooterComponent);
      }

      _createClass(FooterComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return FooterComponent;
    }();

    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-footer',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./footer.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/footer/footer.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./footer.component.scss */
      "./src/app/components/footer/footer.component.scss")).default]
    })], FooterComponent);
    /***/
  },

  /***/
  "./src/app/components/header/header.component.scss":
  /*!*********************************************************!*\
    !*** ./src/app/components/header/header.component.scss ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsHeaderHeaderComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".fill-remaining-space {\n  flex: 1 1 auto;\n}\n\na:hover {\n  color: unset;\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvRTpcXHdvcmtzcGFjZVxcc2NydW10cmFja2VyXFxzY3J1bXRyYWNrZXItZnJvbnRlbmQvc3JjXFxhcHBcXGNvbXBvbmVudHNcXGhlYWRlclxcaGVhZGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxjQUFBO0FDQ0Y7O0FERUE7RUFDRSxZQUFBO0VBQ0EscUJBQUE7QUNDRiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5maWxsLXJlbWFpbmluZy1zcGFjZSB7XHJcbiAgZmxleDogMSAxIGF1dG87XHJcbn1cclxuXHJcbmE6aG92ZXIge1xyXG4gIGNvbG9yOiB1bnNldDtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuIiwiLmZpbGwtcmVtYWluaW5nLXNwYWNlIHtcbiAgZmxleDogMSAxIGF1dG87XG59XG5cbmE6aG92ZXIge1xuICBjb2xvcjogdW5zZXQ7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/components/header/header.component.ts":
  /*!*******************************************************!*\
    !*** ./src/app/components/header/header.component.ts ***!
    \*******************************************************/

  /*! exports provided: HeaderComponent */

  /***/
  function srcAppComponentsHeaderHeaderComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HeaderComponent", function () {
      return HeaderComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_user_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/user.service */
    "./src/app/services/user.service.ts");

    var HeaderComponent =
    /*#__PURE__*/
    function () {
      function HeaderComponent(userService) {
        _classCallCheck(this, HeaderComponent);

        this.userService = userService;
      }

      _createClass(HeaderComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }, {
        key: "signInWithGoogle",
        value: function signInWithGoogle() {
          this.userService.signInWithGoogle();
        }
      }, {
        key: "signOutWithGoogle",
        value: function signOutWithGoogle() {
          this.userService.signOutWithGoogle();
        }
      }, {
        key: "getUser",
        value: function getUser() {
          return this.userService.user;
        }
      }]);

      return HeaderComponent;
    }();

    HeaderComponent.ctorParameters = function () {
      return [{
        type: _services_user_service__WEBPACK_IMPORTED_MODULE_2__["UserService"]
      }];
    };

    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-header',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./header.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./header.component.scss */
      "./src/app/components/header/header.component.scss")).default]
    })], HeaderComponent);
    /***/
  },

  /***/
  "./src/app/components/sprints-history/sprints-history.component.scss":
  /*!***************************************************************************!*\
    !*** ./src/app/components/sprints-history/sprints-history.component.scss ***!
    \***************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsSprintsHistorySprintsHistoryComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvc3ByaW50cy1oaXN0b3J5L3NwcmludHMtaGlzdG9yeS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/components/sprints-history/sprints-history.component.ts":
  /*!*************************************************************************!*\
    !*** ./src/app/components/sprints-history/sprints-history.component.ts ***!
    \*************************************************************************/

  /*! exports provided: SprintsHistoryComponent */

  /***/
  function srcAppComponentsSprintsHistorySprintsHistoryComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SprintsHistoryComponent", function () {
      return SprintsHistoryComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


      var _services_user_story_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
          /*! ../../services/user-story.service */
          "./src/app/services/user-story.service.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
          /*! @angular/router */
          "./node_modules/@angular/router/fesm2015/router.js");
      /**
       * Zeige Tabelle mit allen Stories vergangener Sprints. Klick auf eine Story öffnet Story Details
       */


      var SprintsHistoryComponent =
          /*#__PURE__*/
          function () {
              function SprintsHistoryComponent(sprintService, route) {
                  var _this9 = this;

                  _classCallCheck(this, SprintsHistoryComponent);

                  this.sprintService = sprintService;
                  this.route = route;
                  this.displayedColumns = ['sprintNumber', 'storyNumber', 'title'];
                  route.params.subscribe(function (params) {
                      _this9.projectId = _this9.route.snapshot.params['projectId'];
                  });
              }

              _createClass(SprintsHistoryComponent, [{
                  key: "ngOnInit",
                  value: function ngOnInit() {
                      var _this10 = this;

                      this.sprintService.getStories({
                          projectId: this.projectId,
                          assignedToSprint: true
                      }).subscribe(function (sprints) {
                          _this10.allStories = sprints;
                      });
                  }
              }]);

              return SprintsHistoryComponent;
          }();

    SprintsHistoryComponent.ctorParameters = function () {
      return [{
        type: _services_user_story_service__WEBPACK_IMPORTED_MODULE_2__["UserStoryService"]
      }, {
        type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
      }];
    };

    SprintsHistoryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sprints-history',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sprints-history.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/sprints-history/sprints-history.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sprints-history.component.scss */
      "./src/app/components/sprints-history/sprints-history.component.scss")).default]
    })], SprintsHistoryComponent);
    /***/
  },

  /***/
  "./src/app/components/story-details/story-details.component.scss":
  /*!***********************************************************************!*\
    !*** ./src/app/components/story-details/story-details.component.scss ***!
    \***********************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppComponentsStoryDetailsStoryDetailsComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "label {\n  font-weight: bold;\n  margin-right: 8px;\n}\n\n.tasks-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.tasks-full-width {\n  width: 100%;\n}\n\n.task-done {\n  text-decoration: line-through;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zdG9yeS1kZXRhaWxzL0U6XFx3b3Jrc3BhY2VcXHNjcnVtdHJhY2tlclxcc2NydW10cmFja2VyLWZyb250ZW5kL3NyY1xcYXBwXFxjb21wb25lbnRzXFxzdG9yeS1kZXRhaWxzXFxzdG9yeS1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9jb21wb25lbnRzL3N0b3J5LWRldGFpbHMvc3RvcnktZGV0YWlscy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7QUNDRjs7QURFQTtFQUNFLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FDQ0Y7O0FERUE7RUFDRSxXQUFBO0FDQ0Y7O0FERUE7RUFDRSw2QkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9zdG9yeS1kZXRhaWxzL3N0b3J5LWRldGFpbHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJsYWJlbCB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XHJcbn1cclxuXHJcbi50YXNrcy1mb3JtIHtcclxuICBtaW4td2lkdGg6IDE1MHB4O1xyXG4gIG1heC13aWR0aDogNTAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi50YXNrcy1mdWxsLXdpZHRoIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnRhc2stZG9uZSB7XHJcbiAgdGV4dC1kZWNvcmF0aW9uOiBsaW5lLXRocm91Z2g7XHJcbn1cclxuIiwibGFiZWwge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLXJpZ2h0OiA4cHg7XG59XG5cbi50YXNrcy1mb3JtIHtcbiAgbWluLXdpZHRoOiAxNTBweDtcbiAgbWF4LXdpZHRoOiA1MDBweDtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi50YXNrcy1mdWxsLXdpZHRoIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi50YXNrLWRvbmUge1xuICB0ZXh0LWRlY29yYXRpb246IGxpbmUtdGhyb3VnaDtcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/components/story-details/story-details.component.ts":
  /*!*********************************************************************!*\
    !*** ./src/app/components/story-details/story-details.component.ts ***!
    \*********************************************************************/

  /*! exports provided: StoryDetailsComponent */

  /***/
  function srcAppComponentsStoryDetailsStoryDetailsComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StoryDetailsComponent", function () {
      return StoryDetailsComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


      var _services_user_story_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
          /*! ../../services/user-story.service */
          "./src/app/services/user-story.service.ts");
      /* harmony import */


      var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
          /*! @angular/material/snack-bar */
          "./node_modules/@angular/material/esm2015/snack-bar.js");
      /**
       * Zeigt Daten einer Story an und erlaubt das Anlegen und Abhaken von Tasks
       */


      var StoryDetailsComponent =
          /*#__PURE__*/
          function () {
              function StoryDetailsComponent(activatedRoute, userStoryService, _snackBar) {
                  _classCallCheck(this, StoryDetailsComponent);

                  this.activatedRoute = activatedRoute;
                  this.userStoryService = userStoryService;
                  this._snackBar = _snackBar;
                  this.story = {};
              }

              _createClass(StoryDetailsComponent, [{
                  key: "ngOnInit",
                  value: function ngOnInit() {
                      var _this11 = this;

                      // lade projectId und storyId aus der activatedRoute (URL)
                      this.activatedRoute.params.subscribe(function (params) {
                          _this11.userStoryService.getStory({
                              projectId: params.projectId,
                              storyId: params.storyId
                          }).subscribe(function (story) {
                              _this11.story = story;

                              _this11.resetTasks();
                          });
                      });
                  }
              }, {
                  key: "addNewTask",
                  value: function addNewTask() {
                      if (!this.newTaskTitle) {
                          return;
                      }

                      this.tasks.push({
                          title: this.newTaskTitle
                      });
                      this.newTaskTitle = '';
                  }
              }, {
                  key: "saveTasks",
                  value: function saveTasks() {
                      var _this12 = this;

                      this.story.tasks = this.tasks;
                      this.userStoryService.updateStory(this.story).subscribe(function () {
                          _this12._snackBar.open('Tasks saved successfully', null, {
                              duration: 2000
                          });
                      });
                  }
              }, {
                  key: "resetTasks",
                  value: function resetTasks() {
                      this.tasks = JSON.parse(JSON.stringify(this.story.tasks));
                  }
              }, {
                  key: "deleteTask",
                  value: function deleteTask(index) {
                      this.tasks.splice(index, 1);
                  }
              }, {
                  key: "markTaskAsDone",
                  value: function markTaskAsDone(i, state) {
                      this.tasks[i].done = state;
                  }
              }]);

              return StoryDetailsComponent;
          }();

    StoryDetailsComponent.ctorParameters = function () {
      return [{
        type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
      }, {
        type: _services_user_story_service__WEBPACK_IMPORTED_MODULE_3__["UserStoryService"]
      }, {
        type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]
      }];
    };

    StoryDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-sprint-details',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./story-details.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/components/story-details/story-details.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./story-details.component.scss */
      "./src/app/components/story-details/story-details.component.scss")).default]
    })], StoryDetailsComponent);
    /***/
  },

  /***/
  "./src/app/http-outgoing.interceptor.ts":
  /*!**********************************************!*\
    !*** ./src/app/http-outgoing.interceptor.ts ***!
    \**********************************************/

  /*! exports provided: TokenInterceptor */

  /***/
  function srcAppHttpOutgoingInterceptorTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function () {
      return TokenInterceptor;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var TokenInterceptor =
    /*#__PURE__*/
    function () {
      function TokenInterceptor() {
        _classCallCheck(this, TokenInterceptor);
      }

      _createClass(TokenInterceptor, [{
        key: "intercept",
        value: function intercept(request, next) {
          var token = localStorage.getItem('token');
          request = request.clone({
            setHeaders: {
              Authorization: "".concat(token)
            }
          });
          return next.handle(request);
        }
      }]);

      return TokenInterceptor;
    }();

    TokenInterceptor = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], TokenInterceptor);
    /***/
  },

  /***/
  "./src/app/services/sprint.service.ts":
  /*!********************************************!*\
    !*** ./src/app/services/sprint.service.ts ***!
    \********************************************/

  /*! exports provided: SprintService */

  /***/
  function srcAppServicesSprintServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SprintService", function () {
      return SprintService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var SprintService =
    /*#__PURE__*/
    function () {
      function SprintService(httpClient) {
        _classCallCheck(this, SprintService);

        this.httpClient = httpClient;
      }

      _createClass(SprintService, [{
        key: "getCurrentSprint",
        value: function getCurrentSprint(param) {
          return this.httpClient.get("/api/v1/projects/".concat(param.projectId, "/sprints/current"));
        }
      }, {
        key: "addStoryToSprint",
        value: function addStoryToSprint(param) {
          var _param$sprint = param.sprint,
              projectId = _param$sprint.projectId,
              sprintNumber = _param$sprint.sprintNumber;
          return this.httpClient.post("/api/v1/projects/".concat(projectId, "/sprints/").concat(sprintNumber, "/stories"), param.story);
        }
      }, {
        key: "removeStoryFromSprint",
        value: function removeStoryFromSprint(param) {
          var _param$sprint2 = param.sprint,
              projectId = _param$sprint2.projectId,
              sprintNumber = _param$sprint2.sprintNumber;
          return this.httpClient.delete("/api/v1/projects/".concat(projectId, "/sprints/").concat(sprintNumber, "/stories/").concat(param.story.storyNumber));
        }
      }, {
        key: "createNewSprint",
        value: function createNewSprint(param) {
          var _param$sprint3 = param.sprint,
              projectId = _param$sprint3.projectId,
              sprintNumber = _param$sprint3.sprintNumber;
          return this.httpClient.post("/api/v1/projects/".concat(projectId, "/sprints"), {
            sprintNumber: sprintNumber + 1
          });
        }
      }]);

      return SprintService;
    }();

    SprintService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    SprintService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], SprintService);
    /***/
  },

  /***/
  "./src/app/services/user-story.service.ts":
  /*!************************************************!*\
    !*** ./src/app/services/user-story.service.ts ***!
    \************************************************/

  /*! exports provided: UserStoryService */

  /***/
  function srcAppServicesUserStoryServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserStoryService", function () {
      return UserStoryService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");

    var UserStoryService =
    /*#__PURE__*/
    function () {
      function UserStoryService(httpClient) {
        _classCallCheck(this, UserStoryService);

        this.httpClient = httpClient;
      }

      _createClass(UserStoryService, [{
        key: "getStories",
        value: function getStories(_ref) {
          var projectId = _ref.projectId,
              assignedToSprint = _ref.assignedToSprint;
          return this.httpClient.get("/api/v1/projects/".concat(projectId, "/stories?assignedToSprint=").concat(assignedToSprint));
        }
      }, {
        key: "getStory",
        value: function getStory(_ref2) {
          var storyId = _ref2.storyId,
              projectId = _ref2.projectId;
          return this.httpClient.get("/api/v1/projects/".concat(projectId, "/stories/").concat(storyId));
        }
      }, {
        key: "createStory",
        value: function createStory(projectId, story) {
          return this.httpClient.post("/api/v1/projects/".concat(projectId, "/stories"), story);
        }
      }, {
        key: "updateStory",
        value: function updateStory(story) {
          return this.httpClient.put("/api/v1/projects/".concat(story.projectId, "/stories/").concat(story.storyNumber), story);
        }
      }]);

      return UserStoryService;
    }();

    UserStoryService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    UserStoryService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], UserStoryService);
    /***/
  },

  /***/
  "./src/app/services/user.service.ts":
  /*!******************************************!*\
    !*** ./src/app/services/user.service.ts ***!
    \******************************************/

  /*! exports provided: UserService */

  /***/
  function srcAppServicesUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserService", function () {
      return UserService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
          /*! @angular/core */
          "./node_modules/@angular/core/fesm2015/core.js");
      /* harmony import */


      var angularx_social_login__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
          /*! angularx-social-login */
          "./node_modules/angularx-social-login/angularx-social-login.js");
      /**
       * UserService verbindet unsere App mit dem AuthService vom Angular Social Login Plugin
       */


      var UserService =
          /*#__PURE__*/
          function () {
              function UserService(authService) {
                  var _this13 = this;

                  _classCallCheck(this, UserService);

                  this.authService = authService;
                  this.authService.authState.subscribe(function (user) {
                      _this13.user = user;
                      localStorage.setItem('userId', user ? user.id : null);
                      localStorage.setItem('token', user ? user.idToken : null);
                  });
              }

              _createClass(UserService, [{
                  key: "signInWithGoogle",
                  value: function signInWithGoogle() {
                      this.authService.signIn(angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["GoogleLoginProvider"].PROVIDER_ID);
                  }
              }, {
                  key: "signOutWithGoogle",
                  value: function signOutWithGoogle() {
                      this.authService.signOut();
                  }
              }]);

              return UserService;
          }();

    UserService.ctorParameters = function () {
      return [{
        type: angularx_social_login__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
      }];
    };

    UserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], UserService);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"]).catch(function (err) {
      return console.error(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! E:\workspace\scrumtracker\scrumtracker-frontend\src\main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map
