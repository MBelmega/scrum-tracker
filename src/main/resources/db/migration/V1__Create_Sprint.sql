CREATE TABLE sprint
(
    project_id    VARCHAR(63) NOT NULL,
    sprint_number INT         NOT NULL,
    PRIMARY KEY (project_id, sprint_number)
)
