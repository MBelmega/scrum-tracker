CREATE TABLE user_story
(
    project_id    VARCHAR(63)  NOT NULL,
    story_number  INT          NOT NULL,
    requester     VARCHAR(255) NOT NULL,
    title         VARCHAR(255) NOT NULL,
    description   MEDIUMTEXT   NOT NULL,
    sprint_number INT          NULL,
    PRIMARY KEY (project_id, story_number)
)
