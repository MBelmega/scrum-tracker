CREATE TABLE task
(
    project_id   VARCHAR(63)  NOT NULL,
    story_number INT          NOT NULL,
    title        VARCHAR(255) NOT NULL,
    done         TINYINT      NOT NULL
)
