package de.oncampus.scrumtracker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

/**
 * Startet die Spring Boot Application, fährt embedded Tomcat-Server auf Port 8080 hoch
 */
@SpringBootApplication
class ScrumtrackerApplication

fun main(args: Array<String>) {
    runApplication<ScrumtrackerApplication>(*args)
}
