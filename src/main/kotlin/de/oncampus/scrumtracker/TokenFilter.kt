package de.oncampus.scrumtracker

import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import org.springframework.context.annotation.Profile
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.io.IOException
import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Jedes eingehende http-Request geht durch den Filter.
 * Der Tokenfilter lässt nur Requests durch, die einen validen Token von Google Signin im Header haben.
 */
@Profile("!test") // Nutze diesen Filter nur außerhalb der Tests
@Component
@Order(1)
class TokenFilter : Filter {

    private val jacksonFactory = JacksonFactory()
    private val transport = NetHttpTransport.Builder().build()

    // ClientId angelegt in der Google Development Console (Website)
    private val sprintTrackerGoogleClientId = listOf("410600106727-c8l3pilsj8ln9co41jf6c9ma7tgf703i.apps.googleusercontent.com")


    @Throws(IOException::class, ServletException::class)
    override fun doFilter(
            request: ServletRequest,
            response: ServletResponse,
            chain: FilterChain) {

        val req = request as HttpServletRequest

        if (!req.servletPath.contains("/api")) {
            chain.doFilter(request, response)
            return // ignore calls that are not for backend
        }

        val idToken = req.getHeader("Authorization")

        // Rufe Google Server auf, um zu bestätigen dass Token echt ist
        val verifier = GoogleIdTokenVerifier.Builder(transport, jacksonFactory)
                .setAudience(sprintTrackerGoogleClientId)
                .build()

        val userToken =
                if (idToken != null) verifier.verify(idToken)
                else null

        // Wenn Token echt ist, lies Email-Adresse des Users aus und schick den Request weiter zum Ziel-Controller
        if (userToken != null) {
            request.setAttribute("userEmail", userToken.payload.email)
            println("AUTHENTICATED ${userToken.payload.email}")
            chain.doFilter(request, response)
        } else {
            // Wenn Token nicht verifiziert werden konnte, weise den Request zurück mit 401 UNAUTHORIZED
            (response as HttpServletResponse).sendError(
                    HttpServletResponse.SC_UNAUTHORIZED,
                    "No valid token.")
        }

    }

}


/**
 * For UnitTests, pretend authentication
 */
@Profile("test")
@Component
@Order(1)
class TestTokenFilter : Filter {

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(
            request: ServletRequest,
            response: ServletResponse,
            chain: FilterChain) {

        request.setAttribute("userEmail", "testUser")
        chain.doFilter(request, response)
    }

}
