package de.oncampus.scrumtracker

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class SprintsController(
        private val sprintsRepository: SprintsRepository,
        private val userStoryRepository: UserStoryRepository
) {

    /**
     * Handlet requests um alle Sprints für ein Projekt zu laden
     */
    @GetMapping("api/v1/projects/{id}/sprints")
    fun getSprintsForProject(
            @PathVariable id: String
    ): List<Sprint> {
        return sprintsRepository.findByProjectIdOrderBySprintNumberDesc(id)
    }

    /**
     * Lade Sprint mir höchster SprintNumber für ein Projekt, inkl. UserStories
     */
    @GetMapping("api/v1/projects/{id}/sprints/current")
    fun getCurrentSprintForProject(
            @PathVariable id: String
    ): SprintResponse? {
        val sprint = sprintsRepository.findFirstByProjectIdOrderBySprintNumberDesc(id)
        return sprint?.let {
            val userStoriesForSprint = userStoryRepository.findByProjectIdAndSprintNumber(
                    projectId = sprint.projectId,
                    sprintNumber = sprint.sprintNumber
            )
            SprintResponse(
                    sprintNumber = it.sprintNumber,
                    projectId = it.projectId,
                    userStories = userStoriesForSprint.map {
                        UserStoryResponse(
                                it.storyNumber,
                                it.requester,
                                it.title,
                                it.description,
                                it.sprintNumber,
                                it.tasks.map {
                                    TaskResponse(
                                            it.title,
                                            it.done
                                    )
                                }
                        )
                    }
            )
        }
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("api/v1/projects/{id}/sprints")
    fun createSprintForProject(
            @PathVariable id: String,
            @RequestBody request: SprintCreateRequest
    ): Sprint {
        sprintsRepository.findByProjectIdOrderBySprintNumberDesc(id).firstOrNull()?.let {
            if (it.sprintNumber + 1 != request.sprintNumber)
                throw BadRequestException("Latest sprint of project $id is ${it.sprintNumber}," +
                        " cannot create sprint ${request.sprintNumber}")
        }
        sprintsRepository.save(Sprint(id, request.sprintNumber))
        return Sprint(id, request.sprintNumber)
    }

    @PostMapping("api/v1/projects/{projectId}/sprints/{sprintNumber}/stories")
    fun addStoryToSprint(
            @PathVariable projectId: String,
            @PathVariable sprintNumber: Long,
            @RequestBody request: AddStoryRequest
    ): UserStory {
        if (projectId != request.projectId)
            throw BadRequestException("The given story is not part of project $projectId")

        val storyToAssign = userStoryRepository.findById(
                UserStoryId(request.projectId, request.storyNumber)
        ).orElseThrow { throw BadRequestException("The given story does not exist.") }

        if (storyToAssign.sprintNumber != null)
            throw BadRequestException("The given story is already assigned to a sprint")

        return userStoryRepository.save(storyToAssign.copy(sprintNumber = sprintNumber))
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("api/v1/projects/{projectId}/sprints/{sprintNumber}/stories/{storyNumber}")
    fun removeStoryFromSprint(
            @PathVariable projectId: String,
            @PathVariable sprintNumber: Long,
            @PathVariable storyNumber: Long
    ) {
        val storyToRemove = userStoryRepository.findById(
                UserStoryId(projectId, storyNumber)
        ).orElseThrow { throw BadRequestException("The given story does not exist.") }

        if (storyToRemove.sprintNumber != sprintNumber)
            throw BadRequestException("The given story is not assigned to sprint $sprintNumber")

        userStoryRepository.save(storyToRemove.copy(sprintNumber = null))
    }
}

data class TaskResponse(
        val title: String,
        val done: Boolean
)

data class UserStoryResponse(
        val storyNumber: Long,
        val requester: String,
        val title: String,
        val description: String,
        val sprintNumber: Long?,
        val tasks: List<TaskResponse>
)


/**
 * Die Request Datenklassen werden von Spring mit den Daten aus dem Request-Body befüllt.
 */
data class SprintCreateRequest(
        val sprintNumber: Long
)

data class AddStoryRequest(
        val storyNumber: Long,
        val projectId: String
)

/**
 * Die Response Datenklassen werden von Spring in JSON-Daten für den Response Body umgewandelt
 */

data class SprintResponse(
        val sprintNumber: Long,
        val projectId: String,
        val userStories: List<UserStoryResponse>
)
