package de.oncampus.scrumtracker

import org.springframework.data.jpa.repository.JpaRepository
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.IdClass

/**
 * Repositories sind Interfaces, Spring Data Jpa generiert daraus automatisch SQL-Abfragen
 */
interface SprintsRepository : JpaRepository<Sprint, SprintId> {
    fun findByProjectIdOrderBySprintNumberDesc(id: String): List<Sprint>
    fun findFirstByProjectIdOrderBySprintNumberDesc(id: String): Sprint?
}

/**
 * Entities entsprechen Datenbanktabellen mit entsprechenden Spalten
 */
@Entity
@IdClass(SprintId::class)
data class Sprint(
        @Id val projectId: String,
        @Id val sprintNumber: Long
) {

}

data class SprintId(
        val projectId: String? = null,
        val sprintNumber: Long? = null
): Serializable
