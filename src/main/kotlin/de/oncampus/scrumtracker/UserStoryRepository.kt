package de.oncampus.scrumtracker

import org.springframework.data.jpa.repository.JpaRepository
import java.io.Serializable
import javax.persistence.*

interface UserStoryRepository : JpaRepository<UserStory, UserStoryId> {
    fun findFirstByProjectIdOrderByStoryNumberDesc(projectId: String): UserStory?
    fun findByProjectIdOrderBySprintNumberDescStoryNumberDesc(projectId: String): List<UserStory>
    fun findByProjectIdAndSprintNumber(projectId: String, sprintNumber: Long): Set<UserStory>
}

@Entity
@IdClass(UserStoryId::class)
data class UserStory(
        @Id val projectId: String,
        @Id val storyNumber: Long,
        val requester: String,
        val title: String,
        val description: String,
        val sprintNumber: Long? = null,

        @ElementCollection(fetch = FetchType.EAGER)
        @CollectionTable(name = "task", joinColumns = [
            JoinColumn(name = "projectId"),
            JoinColumn(name = "storyNumber")
        ]
        )
        val tasks: List<Task> = emptyList()
) {

}

data class UserStoryId(
        val projectId: String? = null,
        val storyNumber: Long? = null
) : Serializable

@Embeddable
data class Task(
        val title: String,
        val done: Boolean = false
)
