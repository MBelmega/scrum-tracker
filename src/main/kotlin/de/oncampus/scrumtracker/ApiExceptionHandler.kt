package de.oncampus.scrumtracker

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus.BAD_REQUEST
import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import javax.servlet.http.HttpServletRequest

/**
 * Fängt alle Exceptions, die nirgends sonst gefangen werden (try/catch).
 * Wandelt die Exceptions in eine Http-Response mit passendem Status Code und Error Message um
 */
@ControllerAdvice
class ApiExceptionHandler {

    @ExceptionHandler(Throwable::class)
    fun handleError(request: HttpServletRequest, exception: Throwable): ResponseEntity<ErrorInfo> {

        val cause = exception.cause
        val (code, message) = when (cause) {
            is BadRequestException -> BAD_REQUEST to cause.message
            is NotFoundException -> NOT_FOUND to cause.message

            else -> throw exception
        }

        val errorInfo = ErrorInfo(error = message!!, path = request.requestURI)

        val headers = HttpHeaders()
        headers.add(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)

        return ResponseEntity(errorInfo, headers, code)
    }

}

data class ErrorInfo(
    val error: String,
    val path: String
)

class BadRequestException(message: String) : Throwable(message)
class NotFoundException(message: String) : Throwable(message)
