package de.oncampus.scrumtracker

import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

/**
 * Erzeugt beim Start der Anwendung Testdaten in die Datenbank, damit wir ein gefülltes Frontend haben
 */
@Component
class TestDataGenerator(
        private val sprintsRepository: SprintsRepository,
        private val userStoryRepository: UserStoryRepository
) {

    private val storyDescription = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."

    @PostConstruct
    fun generateTestData() {
        sprintsRepository.saveAll(listOf(
                Sprint(
                        projectId = "1",
                        sprintNumber = 1
                ),
                Sprint(
                        projectId = "1",
                        sprintNumber = 2
                ),
                Sprint(
                        projectId = "1",
                        sprintNumber = 3
                )
        ))

        userStoryRepository.saveAll(listOf(
                UserStory(
                        projectId = "1",
                        storyNumber = 1,
                        requester = "Maja",
                        title = "User can see the history of all stories",
                        description = storyDescription,
                        sprintNumber = 1
                ),
                UserStory(
                        projectId = "1",
                        storyNumber = 2,
                        requester = "Maja",
                        title = "User view the details of a story",
                        description = storyDescription,
                        sprintNumber = 1
                ),
                UserStory(
                        projectId = "1",
                        storyNumber = 3,
                        requester = "Maja",
                        title = "User can see the list of stories not assigned to a sprint",
                        description = storyDescription,
                        sprintNumber = 2
                ),
                UserStory(
                        projectId = "1",
                        storyNumber = 4,
                        requester = "Maja",
                        title = "User can create a new sprint",
                        description = storyDescription,
                        sprintNumber = 2
                ),
                UserStory(
                        projectId = "1",
                        storyNumber = 5,
                        requester = "Maja",
                        title = "User can assign stories to a sprint",
                        description = storyDescription
                ),
                UserStory(
                        projectId = "1",
                        storyNumber = 6,
                        requester = "Maja",
                        title = "User can create a new story",
                        description = storyDescription
                ),
                UserStory(
                        projectId = "1",
                        storyNumber = 7,
                        requester = "That other guy",
                        title = "User can see the stories assigned to the current sprint",
                        description = storyDescription,
                        sprintNumber = 3
                )
        ))
    }
}
