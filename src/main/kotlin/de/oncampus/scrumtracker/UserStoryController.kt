package de.oncampus.scrumtracker

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
class UserStoryController(
        val userStoryRepository: UserStoryRepository
) {

    @PostMapping("/api/v1/projects/{projectId}/stories")
    @ResponseStatus(HttpStatus.CREATED)
    fun createUserStory(
            @PathVariable projectId: String,
            @RequestBody request: StoryCreateRequest,
            @RequestAttribute userEmail: String
    ) {
        val latestStory =
                userStoryRepository.findFirstByProjectIdOrderByStoryNumberDesc(projectId)
        userStoryRepository.save(UserStory(
                projectId = projectId,
                storyNumber = (latestStory?.storyNumber ?: 0) + 1,
                requester = userEmail,
                title = request.title,
                description = request.description
        ))
    }

    @GetMapping("/api/v1/projects/{projectId}/stories")
    fun getUserStoriesForProject(
            @PathVariable projectId: String,
            @RequestParam(required = false) assignedToSprint: Boolean?
    ): List<UserStory> {
        val stories = userStoryRepository
                .findByProjectIdOrderBySprintNumberDescStoryNumberDesc(projectId)
        return stories
                .filter { assignedToSprint == null
                        || (!assignedToSprint && it.sprintNumber == null)
                        || (assignedToSprint && it.sprintNumber != null)
                }
    }

    @GetMapping("/api/v1/projects/{projectId}/stories/{storyNumber}")
    fun getUserStory(
            @PathVariable projectId: String,
            @PathVariable storyNumber: Long
    ): UserStory {
        val story = userStoryRepository
                .findById(UserStoryId(projectId, storyNumber))
        return story.orElseThrow { NotFoundException(
                "Story $storyNumber not found in project $projectId"
        ) }
    }

    @PutMapping("/api/v1/projects/{projectId}/stories/{storyNumber}")
    fun updateUserStory(
            @PathVariable projectId: String,
            @PathVariable storyNumber: Long,
            @RequestBody request: StoryUpdateRequest
    ): UserStory {
        val story = userStoryRepository
                .findById(UserStoryId(projectId, storyNumber))
                .orElseThrow { NotFoundException(
                "Story $storyNumber not found in project $projectId"
        ) }
        val updatedStory = story.copy(
                title = request.title,
                description = request.description,
                tasks = request.tasks.map {
                    Task(it.title, it.done)
                }
        )
        userStoryRepository.save(updatedStory)
        return updatedStory
    }


}

data class StoryCreateRequest(
        val title: String,
        val description: String
)

data class StoryUpdateRequest(
        val title: String,
        val description: String,
        val tasks: List<TaskRequest>
)

data class TaskRequest(
        val title: String,
        val done: Boolean
)
