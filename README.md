# scrumtracker

To build and run all tests, type `gradlew clean build`
To start the application, type `gradlew clean build bootRun -xtest`

The app starts on localhost:8080

## Architektur

###Zwei Schichten:
- Presentation: Controller nehmen http-Requests vom Frontend entgegen, verarbeiten und erzeugen  Antwort
- Data Access: Repositories speichern Entities in Datenbank / laden aus Datenbank 

### Datenbank:
h2 in-memory SQL-Datenbank. Muss nicht extern installiert werden, sondern wird als Gradle-Dependency geladen und beim Start der Anwendung im Arbeitsspeicher hochgefahren.
Konfiguration in  application.properties 

### REST-Endpunkte:
- GET lade alle Objekte oder lade ein Objekt by Id
- POST erzeuge ein neues Objekz
- PUT update ein Objekt
- DELETE lösche ein Objekt

## Google Login
- Im Frontend mit Angular Social Login Plugin, UserService zum Login/Logout
- Speichert UserDaten / Token im LocalStorage des Browsers
- HttpOutgoingInterceptor schreibt den Google-Token aus dem LocalStorage in jedes Request zum Backend
- TokenFilter im Backend lässt nur Requests mit gültigem Token durch

## Frontend
- Angular Material Design für Tabs, Tabellen, Buttons, Input
- SprintService (Frontend) kommuniziert mit SprintController (Backend)
- UserStoryService (Frontend) kommuniziert mit UserStoryController (Backend)
- model Klassen enthalten die geladenen Daten aus dem Backend. Entsprechen den Response data classes im Backend

