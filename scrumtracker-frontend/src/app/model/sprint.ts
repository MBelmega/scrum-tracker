import {UserStory} from './user-story';

export interface Sprint {
  projectId: string;
  sprintNumber: number;
  userStories: UserStory[];
}
