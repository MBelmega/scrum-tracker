import { Task } from './task';

export interface UserStory {
  projectId: string;
  sprintNumber: number;
  storyNumber: number;
  requester: string;
  title: string;
  description: string;
  tasks: Task[];
}
