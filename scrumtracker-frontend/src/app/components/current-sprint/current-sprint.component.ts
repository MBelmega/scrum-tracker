import {Component, OnInit} from '@angular/core';
import {UserStory} from '../../model/user-story';
import {UserStoryService} from '../../services/user-story.service';
import {Sprint} from '../../model/sprint';
import {SprintService} from '../../services/sprint.service';
import {ActivatedRoute} from '@angular/router';

/**
 * Zeigt zwei Tabellen, eine für Aktuellen Sprint und eine für nicht zugeordnete Stories.
 * Nutzer kann Stories aus der zweiten Tabelle dem Sprint zuordnen
 */
@Component({
  selector: 'app-current-sprint',
  templateUrl: './current-sprint.component.html',
  styleUrls: ['./current-sprint.component.scss']
})
export class CurrentSprintComponent implements OnInit {

  freeStories: UserStory[];
  displayedColumns: string[] = ['storyNumber', 'title', 'actions'];
  currentSprint: Sprint;
  projectId: string;

  constructor(
    private userStoryService: UserStoryService,
    private sprintService: SprintService,
    private route: ActivatedRoute
  ) {
    route.params.subscribe((params) => {
      this.projectId = this.route.snapshot.params['projectId'];
    });
  }

  ngOnInit() {
    this.loadCurrentSprint();
    this.loadFreeStories();
  }

  private loadFreeStories() {
    this.userStoryService.getStories({projectId: this.projectId, assignedToSprint: false}).subscribe((stories) => {
      this.freeStories = stories;
    });
  }

  private loadCurrentSprint() {
    this.sprintService.getCurrentSprint({projectId: this.projectId}).subscribe((sprint) => {
      this.currentSprint = sprint;
    });
  }

  addToCurrentSprint(element: UserStory) {
    this.sprintService.addStoryToSprint({sprint: this.currentSprint, story: element}).subscribe(() => {
      this.loadCurrentSprint();
      this.loadFreeStories();
    });
  }

  removeFromCurrentSprint(element: UserStory) {
    this.sprintService.removeStoryFromSprint({sprint: this.currentSprint, story: element}).subscribe(() => {
      this.loadCurrentSprint();
      this.loadFreeStories();
    });
  }

  startNextSprint() {
    this.sprintService.createNewSprint({sprint: this.currentSprint}).subscribe(() => {
      this.loadCurrentSprint();
    });
  }
}
