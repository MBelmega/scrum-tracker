import {Component, OnInit} from '@angular/core';
import {UserStory} from '../../model/user-story';
import {UserStoryService} from '../../services/user-story.service';
import {ActivatedRoute} from '@angular/router';

/**
 * Zeige Tabelle mit allen Stories vergangener Sprints. Klick auf eine Story öffnet Story Details
 */
@Component({
  selector: 'app-sprints-history',
  templateUrl: './sprints-history.component.html',
  styleUrls: ['./sprints-history.component.scss']
})
export class SprintsHistoryComponent implements OnInit {
  allStories: UserStory[];
  displayedColumns: string[] = ['sprintNumber', 'storyNumber', 'title'];
  private projectId: string;

  constructor(
    private sprintService: UserStoryService,
    private route: ActivatedRoute
  ) {
    route.params.subscribe((params) => {
      this.projectId = this.route.snapshot.params['projectId'];
    });
  }

  ngOnInit() {
    this.sprintService.getStories({projectId: this.projectId, assignedToSprint: true}).subscribe((sprints) => {
      this.allStories = sprints;
    });
  }

}
