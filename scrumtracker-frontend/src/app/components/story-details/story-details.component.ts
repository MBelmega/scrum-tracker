import {Component, OnInit} from '@angular/core';
import {UserStory} from '../../model/user-story';
import {ActivatedRoute} from '@angular/router';
import {UserStoryService} from '../../services/user-story.service';
import {Task} from 'src/app/model/task';
import {MatSnackBar} from '@angular/material/snack-bar';

/**
 * Zeigt Daten einer Story an und erlaubt das Anlegen und Abhaken von Tasks
 */
@Component({
  selector: 'app-sprint-details',
  templateUrl: './story-details.component.html',
  styleUrls: ['./story-details.component.scss']
})
export class StoryDetailsComponent implements OnInit {

  story: UserStory = {} as any;
  tasks: Task[];
  newTaskTitle: string;
  focusState: boolean;

  constructor(
    private activatedRoute: ActivatedRoute,
    private userStoryService: UserStoryService,
    private _snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    // lade projectId und storyId aus der activatedRoute (URL)
    this.activatedRoute.params.subscribe((params) => {
      this.userStoryService.getStory({
          projectId: params.projectId,
          storyId: params.storyId
        }
      ).subscribe((story: UserStory) => {
        this.story = story;
        this.resetTasks();
      });
    });
  }

  addNewTask() {
    if (!this.newTaskTitle) {
      return;
    }

    this.tasks.push({
      title: this.newTaskTitle
    } as Task);
    this.newTaskTitle = '';
  }

  saveTasks() {
    this.story.tasks = this.tasks;
    this.userStoryService.updateStory(this.story).subscribe(() => {
      this._snackBar.open('Tasks saved successfully', null, {
        duration: 2000,
      });
    });
  }

  resetTasks() {
    this.tasks = JSON.parse(JSON.stringify(this.story.tasks));
  }

  deleteTask(index: number) {
    this.tasks.splice(index, 1);
  }

  markTaskAsDone(i: number, state: boolean) {
    this.tasks[i].done = state;
  }
}
