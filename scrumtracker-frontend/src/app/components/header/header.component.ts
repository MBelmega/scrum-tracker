import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    private userService: UserService
  ) {
  }

  ngOnInit() {

  }

  signInWithGoogle() {
    this.userService.signInWithGoogle();
  }

  signOutWithGoogle() {
    this.userService.signOutWithGoogle();
  }

  getUser() {
    return this.userService.user;
  }
}
