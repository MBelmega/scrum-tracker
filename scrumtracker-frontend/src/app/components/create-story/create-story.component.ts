import {Component, OnInit} from '@angular/core';
import {UserStory} from '../../model/user-story';
import {FormBuilder, Validators} from '@angular/forms';
import {UserStoryService} from '../../services/user-story.service';
import {ActivatedRoute} from '@angular/router';

/**
 * Formular zum Anlegen einer neuen Story
 */
@Component({
  selector: 'app-create-story',
  templateUrl: './create-story.component.html',
  styleUrls: ['./create-story.component.scss']
})
export class CreateStoryComponent implements OnInit {

  story: UserStory;

  form = this.fb.group(
    {
      storyTitle: [null, Validators.required],
      storyDescription: [null, Validators.required]
    }
  );
  private projectId: string;

  constructor(
    private fb: FormBuilder,
    private userStoryService: UserStoryService,
    private route: ActivatedRoute
  ) {
    route.params.subscribe((params) => { // lade projectId aus der URL
      this.projectId = this.route.snapshot.params['projectId'];
    });
  }


  ngOnInit() {
  }

  // Bei Submit-Klick, schicke Request zum Erstellen der Story ans Backend und resette Formular
  onSubmit() {
    let formValue = this.form.getRawValue();
    this.userStoryService.createStory(this.projectId,
      {
        title: formValue.storyTitle,
        description: formValue.storyDescription
      } as UserStory).subscribe(() => {
        this.form.reset();
      }
    );
  }

}
