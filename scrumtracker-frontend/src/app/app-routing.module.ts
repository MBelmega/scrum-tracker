import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {StoryDetailsComponent} from './components/story-details/story-details.component';
import {SprintsHistoryComponent} from './components/sprints-history/sprints-history.component';
import {CreateStoryComponent} from './components/create-story/create-story.component';
import {CurrentSprintComponent} from './components/current-sprint/current-sprint.component';

/**
 * Definiert welche UI-Komponente im Main-Bereich der app-component.html angezeigt wird,
 * abhängig von der aktiven Route. Navigation kann so über Links erfolgen.
 */
const routes: Routes = [
  {
    path: '',
    redirectTo: 'projects/1/sprints/current',
    pathMatch: 'full',
  },
  {
    path: 'projects/:projectId/sprints/current',
    component: CurrentSprintComponent
  },
  {
    path: 'projects/:projectId/sprints/history',
    component: SprintsHistoryComponent
  },
  {
    path: 'projects/:projectId/stories/create',
    component: CreateStoryComponent
  },
  {
    path: 'projects/:projectId/stories/:storyId',
    component: StoryDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
