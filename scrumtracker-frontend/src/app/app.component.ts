import {Component} from '@angular/core';
import {UserService} from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  constructor(
    private userService: UserService
  ) {
  }

  private projectId = '1';

  navLinks = [{
    path: 'projects/' + this.projectId + '/sprints/current',
    label: 'Current Planning'
  }, {
    path: 'projects/' + this.projectId + '/sprints/history',
    label: 'Sprint History'
  }, {
    path: 'projects/' + this.projectId + '/stories/create',
    label: 'Create New Story'
  },
  ];

  userIsLoggedIn() {
    return !!this.userService.user;
  }

  signInWithGoogle() {
    this.userService.signInWithGoogle();
  }
}
