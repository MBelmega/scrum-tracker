import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserStory} from '../model/user-story';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserStoryService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getStories({projectId, assignedToSprint}): Observable<UserStory[]> {
    return this.httpClient.get<UserStory[]>(`/api/v1/projects/${projectId}/stories?assignedToSprint=${assignedToSprint}`);
  }

  getStory({storyId, projectId}) {
    return this.httpClient.get<UserStory>(`/api/v1/projects/${projectId}/stories/${storyId}`);
  }

  createStory(projectId: string, story: UserStory) {
    return this.httpClient.post(
      `/api/v1/projects/${projectId}/stories`,
      story
    )
  }

  updateStory(story: UserStory) {
    return this.httpClient.put(
      `/api/v1/projects/${story.projectId}/stories/${story.storyNumber}`,
      story
    )
  }
}
