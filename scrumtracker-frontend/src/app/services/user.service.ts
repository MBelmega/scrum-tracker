import {Injectable} from '@angular/core';
import {AuthService, GoogleLoginProvider, SocialUser} from 'angularx-social-login';

/**
 * UserService verbindet unsere App mit dem AuthService vom Angular Social Login Plugin
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {

  user: SocialUser;

  constructor(
    private authService: AuthService
  ) {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      localStorage.setItem('userId', user ? user.id : null);
      localStorage.setItem('token', user ? user.idToken : null);
    });
  }

  signInWithGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  signOutWithGoogle() {
    this.authService.signOut();
  }
}
