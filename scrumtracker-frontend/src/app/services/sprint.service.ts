import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Sprint} from '../model/sprint';
import {UserStory} from '../model/user-story';

@Injectable({
  providedIn: 'root'
})
export class SprintService {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getCurrentSprint(param: { projectId: string }): Observable<Sprint> {
    return this.httpClient.get<Sprint>(`/api/v1/projects/${param.projectId}/sprints/current`);
  }

  addStoryToSprint(param: { sprint: Sprint; story: UserStory }): Observable<Object> {
    const {projectId, sprintNumber} = param.sprint;
    return this.httpClient.post(
      `/api/v1/projects/${projectId}/sprints/${sprintNumber}/stories`,
      param.story
    );
  }

  removeStoryFromSprint(param: { sprint: Sprint; story: UserStory }) {
    const {projectId, sprintNumber} = param.sprint;
    return this.httpClient.delete(
      `/api/v1/projects/${projectId}/sprints/${sprintNumber}/stories/${param.story.storyNumber}`
    );
  }

  createNewSprint(param: { sprint: Sprint }): Observable<Object> {
    const {projectId, sprintNumber} = param.sprint;
    return this.httpClient.post(`/api/v1/projects/${projectId}/sprints`,
      {sprintNumber: sprintNumber + 1});
  }
}
